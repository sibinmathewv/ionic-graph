
// import { Injectable } from '@angular/core';
// import {Http} from '@angular/http';
// import 'rxjs/Rx';
// import 'rxjs/add/operator/map';


// /*
//   Generated class for the ApiconnProvider provider.

//   See https://angular.io/guide/dependency-injection for more info on providers
//   and Angular DI.
// */
// @Injectable()
// export class ApiconnProvider {

//   constructor(public http: Http) {
//     console.log('Hello ApiconnProvider Provider');
//   }
// getdata(apiurl):any
// {
//   return this.http.get(apiurl).map(res=>res.json());
// }
// getData(apiUrl):any{
//   console.log("getapiUrl" +apiUrl);
//   return this.http.get(apiUrl).map(res => res.json());
// }
// }
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import {Headers, RequestOptions} from '@angular/http';
import 'rxjs/Rx';
import 'rxjs/add/observable/fromEvent';


@Injectable()
export class ApiconnProvider {
  public mainUrl="http://services.myhabbay.in/1.0/myhabbay/"
  public mainUrl1="http://admins.myhabbay.in/";
  public serviceUrl="http://services.myhabbay.in/myhabbay/";
 
    public items : any
  constructor(public http: Http) {

   
  }
//http.get
   getData(apiUrl):any{
       console.log("getapiUrl" +apiUrl);
       return this.http.get(apiUrl).map(res => res.json());
   }
//http.post
postData(postUrl,postData):any{  
       
      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({ headers: headers }); 
      console.log("posted Data:"+JSON.stringify(postData));
      console.log("Posted url:"+postUrl);
       return this.http.post(postUrl,postData,options).map(res => res.json());
   }

//    filterItems(searchTerm){
//     return this.items.filter((item) => {
//         return item.title.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
//     });    
//    }
   

filterItems() : any{
    return this.items = [
                {title: 'one'},
                {title: 'two'},
                {title: 'three'},
                {title: 'four'},
                {title: 'five'},
                {title: 'six'}
            ]
   }

}
