import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { ContentDrawer } from '../components/content-drawer/content-drawer';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { TablePage} from '../pages/table/table'
import { ApiconnProvider } from '../providers/apiconn/apiconn';
import {HttpModule} from '@angular/http'
import {LoginPage} from '../pages/login/login'
//import { AuthLoginPage } from '../pages/auth-login/auth-login';
// import { AuthLoginPageModule } from '../pages/auth-login/auth-login.module';
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ContentDrawer,
    LoginPage,
    TablePage

 
    
  ],
  imports: [
    BrowserModule,

   // AuthLoginPageModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
  

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    TablePage
 
   
  ],
  providers: [
    StatusBar,
    SplashScreen,
    HttpModule,
    ApiconnProvider
  ]
})
export class AppModule {}
