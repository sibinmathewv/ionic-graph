import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import * as HighCharts from 'highcharts';
//import { AuthLoginPage } from '../auth-login/auth-login';
//import { AuthLoginPageModule } from '../auth-login/auth-login.module';
import {ApiconnProvider} from '../../providers/apiconn/apiconn'
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public slideinfo;
  drawerOptions: any;
public addurl="http://services.myhabbay.in/myhabbay/ads/";
main_api_url="https://newsapi.org/v2/top-headlines?country=us&category=business&apiKey=ca5490b5ff264cf6899c3eb10e762251"
  constructor(public navCtrl: NavController,public api_conn:ApiconnProvider,public api_connection : ApiconnProvider) {
    //this.strt();
   
    this.drawerOptions = {
      handleHeight: 50,
      thresholdFromBottom: 200,
      thresholdFromTop: 200,
      bounceBack: true
  };
  }
  ionViewDidLoad()
  {
  
    this.loadslider();
  var myChart = HighCharts.chart('container', {
    chart: {
    type: 'spline'
    },
    title: {
    text: 'Fruit Consumption'
    },
    xAxis: {
    categories: ['Apples', 'Bananas', 'Oranges','watermelon','grape']
    },
    yAxis: {
    title: {
    text: 'Fruit eaten'
    }
    },
    series: [{
    name: 'Jane',
    data: [1, 0, 4,6,2]
    }, {
    name: 'John',
    data: [5, 7, 3,5,10]
    }]
    });
}
public go()
{
  this.navCtrl.push('login');
}
// public strt()
// {
// var apiurl=this.addurl + 'getsliderads';
// this.api_conn.getdata(apiurl).subscribe(response=>{
//   // if(!response.error){
    
//   //      console.log("slidees"+JSON.stringify(response)) ;
//   //       this.slideinfo=response.value;
//   //       console.log(JSON.stringify(this.slideinfo));
        
//   //      }
//   console.log(JSON.stringify(response));
// });
// }
public loadslider()
{
  
  console.log("slide starttt");
  var getUrl = this.main_api_url;
  this.api_connection.getData(getUrl).subscribe(response =>{
    if(!response.error){

 console.log("slidees"+JSON.stringify(response)) ;
  this.slideinfo=response.value;
  console.log(JSON.stringify(this.slideinfo));
  
 }
}
);
// this.slides.autoplayDisableOnInteraction = false;
}
}
