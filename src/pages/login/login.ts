import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ViewController} from 'ionic-angular';
import {TablePage} from '../table/table'
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  arrays=[]
public Userdetail=
{
  fname:"",
  lname:"",
  email:""
}
  constructor(public navCtrl: NavController, public viewCtrl:ViewController,public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
  
public addinfo()
{
 let array={
    "FName":this.Userdetail.fname,
    "LName":this.Userdetail.lname,
    "EMail":this.Userdetail.email

  }
this.arrays.push(array)
console.log(JSON.stringify(this.arrays));
window.localStorage.setItem("logindetail",JSON.stringify(this.arrays));
}
public next()
{
  this.navCtrl.push(TablePage);
}
}
