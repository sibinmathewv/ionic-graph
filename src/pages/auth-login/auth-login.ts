
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { HomePage } from '../home/home';
 
@IonicPage({
  segment: 'auth-login', // will be used in browser as URL
  name: 'login'
})
@Component({
  selector: 'page-auth-login',
  templateUrl: 'auth-login.html',
})
 
export class AuthLoginPage {
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
 
  goToRegister() {
    this.navCtrl.push('register'); //IonicPage in use
  }
}
